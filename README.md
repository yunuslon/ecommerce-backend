<h1><a href="https://gitlab.com/yunuslon/ecommerce-backend">Screen CMS Ecommers Shayna Admin</a></h1>

## Login
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/Login.png" alt="drawing" width="600"/> 
<p>Login berfungsi mengindentifikasi atau memfilter user yang berhak masuk dalam sistem</p>

## Home
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/Home.png" alt="drawing" width="600"/>
<p>Home berfungsi menampilkan informasi umum hasil penjualan,Seprti </p>
<ol>
  <li>Jumlah Penghasilan</li>
  <li>Jumlah penjualan sukses</li>
  <li>List customer yang melakukan transaksi</li>
  <li>Diagram informasi yang menampilkan tentang Jumlah transaksi panding,transaksi gagal dan transaksi sukses</li>
</ol>

## Daftar Barang
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/daftar-barang.png" alt="drawing" width="600"/>
<p>Fungsi daftar barang merupakan list informasi data dari barang yang tersedia untuk di jual </p>

## Tambah Barang
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/Tambah-barang.png" alt="drawing" width="600"/>
<p>Fungsi Tambah Barang adalah tempat admin menmabahkan barang yang akan di jual</p>

## Daftar Foto Barang
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/df-barang.png" alt="drawing" width="600"/>
<p>Daftar Foto Barang merupakan child dari tabel barang yang berfungsi untuk menampilkan daftar foto barang yang akan di tampilkan pada web Shayna customer</p>

## Tambah Foto Barang
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/tf-barang.png" alt="drawing" width="600"/>
<p>Tambah Foto Barang merupakan child dari tabel barang yang berfungsi untuk menambahkan foto barang oleh admin untuk  di tampilkan pada web Shayna customer</p>

## Daftar Transaksi Masuk
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/d-transaksi-masuk.png" alt="drawing" width="600"/>
<p>Daftar Transaksi Masuk berfungsi untuk menampilkan daftar transaksi yang telah dilakukan oleh customer di web Shayna customer</p>

## Detail Transaksi
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/detail_transaksi.png" alt="drawing" width="600"/>
<p>Detail Transaksi adalah tempat admin mengecek daftar transaksi yang akan di proses, Seprti untuk mengubah Status transaksi menjadi</p>
<ol>
   <li>Sukses</li>
   <li>Gagal</li>
   <li>Pending</li>
</ol>
<h1><a href="https://gitlab.com/yunuslon/ecommerce-shop">Web Sahayana Customer</a></h1>

<hr/>
<br/><br/><br/><br/>
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
